require 'rails_helper'

RSpec.describe Question, :type => :model do

  context 'validates title' do
    # it 'length' do
    #   title = 'a' * 141
    #
    #   expect(Question.new(title: title)).to_not be_valid
    # end
  end
  it { should validate_presence_of :title }
  it { should ensure_length_of(:title).is_at_most(140) }
  it { should validate_presence_of :body }
  it { should have_many :answers }
  it { should have_many :attachments }
  it { should have_many :comments }
  it { should accept_nested_attributes_for :attachments }
  it { should belong_to :user }
end
