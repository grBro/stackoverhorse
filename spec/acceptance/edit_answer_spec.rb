require_relative 'acceptance_helper'

feature 'Edit answer', %q{
  In order to fix mistake
  As an author of answer
  I'd like to be able to edit my answer
} do

  given(:user) { (create :user) }
  given(:another_user) { (create :user) }
  given!(:question) { (create :question) }
  given!(:answer) { (create :answer, question: question, user: user) }

  scenario 'Unauthenticated user trying to edit answer' do
    visit question_path(question)

    expect(page).to_not have_link 'Edit'
  end


  describe 'Authenticated user' do
    before do
      sign_in user
      visit question_path(question)
    end

    scenario 'sees link to Edit' do
      within '.answers' do
        expect(page).to have_link 'Edit'
      end
    end

    scenario 'trying to edit his answer', js: true do
      click_on 'Edit'
      within '.answer-content' do
        fill_in 'Answer', with: 'edited answer'
        click_on 'Save'
        expect(page).to_not have_content answer.content
        expect(page).to have_content 'edited answer'
        expect(page).to_not have_selector 'textarea'
      end
    end
  end

  describe 'Another authenticated user' do
    before do
      sign_in another_user
      visit question_path(question)
    end

    scenario 'trying to edit other user answer' do
      expect(page).to_not have_link 'Edit'
    end
  end




end