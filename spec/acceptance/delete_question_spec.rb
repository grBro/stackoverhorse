require_relative 'acceptance_helper'

feature 'Delete question', %q{
  In order to clean the DB with unwanted content
  As an authenticated user
  I want to be able to delete questions
} do

  given(:user) { create(:user) }
  given!(:question) { (create :question, user: user) }

  scenario 'Authenticated user deletes question' do
    sign_in(user)

    visit questions_path

    expect { click_on 'Delete' }.to change(Question, :count).by(-1)
  end

  scenario 'Non-authenticated tries to delete question' do
    User.create!(email: 'user@test.com', password: '12345678')

    visit questions_path

    expect(page).to_not have_content 'Delete'
  end

end