# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

special_char_convert = HTMLEntities.new

questions_api = RubyStackoverflow.questions(options = {pagesize: 100,
                                                       order: 'desc',
                                                       sort: 'activity',
                                                       tagged: 'ruby',
                                                       filter: '!b0OfNFI0AW51TZ'})

user = User.first

questions_api.data.each do |x|
  begin
  question = user.questions.create(title: special_char_convert.decode(x.title),
                                   body: special_char_convert.decode("#{x.body_markdown}\r\n\r\n\r\n\r\n[Link to original](#{x.link})"),
                                   all_tags: x.tags.join(','))

  if x.comment_count > 0
    x.comments.each do |comment|
      question.comments.create(body: special_char_convert.decode(comment[:body_markdown]))
    end
  end
  rescue Exception => e
    puts "There is an error: #{e.message}"
  ensure
    answers_api = RubyStackoverflow.answers_of_questions([x.question_id], {order: 'desc',
                                                                           sort: 'votes',
                                                                           filter: '!b0OfNYNxht.qQn'})
    if x.is_answered == true
      answers_api.data[0].answers.each do |a|
        answer = question.answers.build(content: special_char_convert.decode(a.body_markdown))
        answer.user = user
        answer.save
        if a.comment_count > 0
          a.comments.each do |comment|
            answer.comments.create(body: special_char_convert.decode(comment[:body_markdown]))
          end
        end
      end
    end
  end
end