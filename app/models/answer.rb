class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :question
  has_many :attachments, as: :attachmentable
  has_many :comments, as: :commentable, dependent: :destroy
  validates :content, presence: true
  validates :user_id, presence: true
  validates :question_id, presence: true
  default_scope -> { order(:created_at) }
  accepts_nested_attributes_for :attachments

  def show_title
    question.title
  end

  def show_object
    self.question
  end

  def show_body
    content
  end
end
