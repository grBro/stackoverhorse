class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  default_scope -> { order(:created_at) }
  validates :body, presence: true

  def show_title
    if commentable.is_a?(Question)
      commentable.title
    else
      commentable.question.title
    end
  end

  def show_object
    if commentable.is_a?(Question)
      self.commentable
    else
      self.commentable.question
    end
  end

  def show_body
    body
  end
end
