class Tag < ActiveRecord::Base
  has_many :taggings
  has_many :questions, through: :taggings, dependent: :destroy

  validates :name, length: { maximum: 40 }
end
