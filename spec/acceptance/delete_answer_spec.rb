require_relative 'acceptance_helper'

feature 'Delete answer', %q{
  In order to clean the DB with unwanted content
  As an authenticated user
  I want to be able to delete answers
} do

  given(:user) { create(:user) }
  given!(:question) { (create :question, user: user) }
  given!(:answer) { (create :answer, question: question, user: user) }

  scenario 'Authenticated user deletes question' do
    sign_in(user)

    visit question_path(question)
    within '.answer-content' do
      expect { click_on 'Delete' }.to change(Answer, :count).by(-1)
    end
  end

  scenario 'Non-authenticated tries to delete question' do
    User.create!(email: 'user@test.com', password: '12345678')

    visit question_path(question)

    expect(page).to_not have_content 'Delete'
  end

end