class VotesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_votable

  respond_to :js

  def update
    case @type
      when 'vote_up'
        respond_with @votable.liked_by current_user
      when 'vote_down'
        respond_with @votable.downvote_from current_user
    end
  end

  private

  def load_votable
    @resource, id, @type = request.path.split('/')[1..3]
    @resource = @resource.singularize
    @votable = @resource.classify.constantize.find(id)
  end
end
