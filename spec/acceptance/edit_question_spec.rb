require_relative 'acceptance_helper'

feature 'Edit question', %q{
  In order to fix mistake
  As an author of question
  I'd like to be able to edit my question
} do

  given(:user) { (create :user) }
  given(:another_user) { (create :user) }
  given!(:question) { (create :question, user: user) }

  scenario 'Unauthenticated user trying to edit question' do
    visit question_path(question)

    expect(page).to_not have_link 'Edit'
  end

  describe 'Authenticated user' do
    before do
      sign_in user
      visit question_path(question)
    end

    scenario 'sees link to Edit' do
      within '.question-body' do
        expect(page).to have_link 'Edit'
      end
    end

    scenario 'trying to edit his answer', js: true do
      click_on 'Edit'
      within '.question-body' do
        fill_in 'Edit your question', with: 'edited question'
        click_on 'Save'
        expect(page).to_not have_content question.body
        expect(page).to have_content 'edited question'
        expect(page).to_not have_selector 'textarea'
      end
    end
  end

  describe 'Another authenticated user' do
    before do
      sign_in another_user
      visit question_path(question)
    end

    scenario 'trying to edit other user question' do
      expect(page).to_not have_link 'Edit'
    end
  end


end