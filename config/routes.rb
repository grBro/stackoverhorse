Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  concern :commentable do
    resources :comments
  end

  concern :votable do
    patch 'vote_up', to: 'votes#update'
    patch 'vote_down', to: 'votes#update'
  end

  resources :questions, concerns: [:votable, :commentable], shallow: true do
    resources :answers, concerns: [:votable, :commentable]
  end

  namespace :api do
    namespace :v1 do
      resources :profiles do
        get :me, on: :collection
      end
      resources :questions
    end
  end

  get 'search/index'

  root to: 'questions#index'

  get 'tags/:tag', to: 'questions#index', as: 'tag'
end
