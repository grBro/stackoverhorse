module VotesHelper
  def vote_up_icon
    content_tag(:i,'',class: 'fa fa-sort-asc fa-3x')
  end

  def vote_down_icon
    content_tag(:i,'',class: 'fa fa-sort-desc fa-3x')
  end
end
