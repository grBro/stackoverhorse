class Question < ActiveRecord::Base
  acts_as_votable
  is_impressionable
  belongs_to :user
  validates :title, presence: true, length: { maximum: 140 }
  validates :body, :user_id, presence: true
  has_many :answers, dependent: :destroy
  has_many :attachments, as: :attachmentable, dependent: :destroy
  has_many :comments, as: :commentable
  has_many :taggings
  has_many :tags, through: :taggings, dependent: :destroy
  accepts_nested_attributes_for :attachments
  scope :desc, -> { order('created_at DESC') }

  def show_object
    self
  end

  def show_title
    self.title
  end

  def show_body
    self.body
  end

  # Virtual attributes for question_params, which
  # received from the form data

  def all_tags=(names)
    self.tags = names.split(",").map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end

  def all_tags
    self.tags.map(&:name).join(", ")
  end

  def self.tagged_with(name)
    Tag.find_by_name!(name).questions
  end

  def score
    self.get_likes.size - self.get_dislikes.size
  end
end
