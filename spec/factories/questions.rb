FactoryGirl.define do
  factory :question do
    title 'Question title'
    body 'Question body'
    association :user, factory: :user
  end

  factory :invalid_question, class: 'Question' do
    title nil
    body nil
    association :user, factory: :user
  end
end
