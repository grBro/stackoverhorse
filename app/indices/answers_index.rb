ThinkingSphinx::Index.define :answer, with: :active_record do
  #fields
  indexes content

  # attributes
  has :id, :as => :answer_id
  has question_id, user_id, created_at, updated_at
end