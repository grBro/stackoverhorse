$ ->
  $(document).on 'click','a.add-comment-link', (e) ->
    e.preventDefault();
    $(@).closest('div').find('form.new_comment').slideToggle()
  $(document).on 'click','#cancel-button', (e) ->
    e.preventDefault();
    $(this).closest('form.new_comment').slideUp()