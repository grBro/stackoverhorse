class AnswersController < ApplicationController
  before_action :authenticate_user!, except: [ :show ]
  before_action :load_answer, only: [ :update, :destroy ]

  respond_to :js

  def create
    @question = Question.find(params[:question_id])
    @answer = @question.answers.build(answer_params)
    @answer.user = current_user
    @answer.save
    respond_with @answer
  end

  def update
    @answer.update(answer_params)
    respond_with @answer
  end

  def destroy
    respond_with(@answer.destroy, location: @question)
  end

  private

  def answer_params
    params.require(:answer).permit(:content, attachments_attributes: [:file])
  end

  def load_answer
    @answer = Answer.find(params[:id])
    @question = @answer.question
  end
end
