ThinkingSphinx::Index.define :comment, with: :active_record do
  #fields
  indexes body
  indexes commentable_type, as: :type, sortable: true

  #attributes
  has commentable_id, created_at, updated_at
end