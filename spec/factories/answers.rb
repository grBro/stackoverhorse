FactoryGirl.define do
  factory :answer do
    content 'MyText'
    #question ''
    association :user, factory: :user
    association :question, factory: :question
  end

  factory :invalid_answer, class: 'Answer' do
    content nil
    association :user, factory: :user
    association :question, factory: :question
  end
end
