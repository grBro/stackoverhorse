class QuestionsController < ApplicationController
  impressionist actions: [:show]
  before_action :load_question, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [ :index, :show]
  before_action :build_answer, only: :show
  before_action :build_question, only: :create

  respond_to :js, only: :destroy

  def index
    if params[:tag]
      respond_with (@questions = Question.tagged_with(params[:tag]).desc.page(params[:page]).per(15))
    else
      respond_with (@questions = Question.desc.page(params[:page]).per(15))
    end
  end

  def show
    respond_with @question
  end

  def new
    respond_with (@question = Question.new)
  end

  def edit
  end

  def create
    respond_with @question
  end

  def update
    if @question.update(question_params)
      @question
    else
      render :edit
    end
  end

  def destroy
    respond_with(@question.destroy)
  end

  private

  def load_question
    @question = Question.find(params[:id])
  end

  def build_answer
    @answer = @question.answers.build
  end

  def build_question
    @user = current_user
    @question = @user.questions.create(question_params)
  end

  def question_params
    params.require(:question).permit(:title, :body, :all_tags, attachments_attributes: [:file])
  end
end
