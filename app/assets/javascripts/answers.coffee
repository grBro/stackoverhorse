# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $(document).on 'click','a.edit-answer-link', (e) ->
    e.preventDefault()
    #answer_id = $(this).data('answerId')
    $(this).closest('ul').find('form.edit_answer').slideToggle()
  $(document).on 'click','#cancel-button', (e) ->
    e.preventDefault()
    $(this).closest('form.edit_answer').slideUp()
