module ApplicationHelper
  def markdown(text)
    renderer = Redcarpet::Render::HTML.new(filter_html:true,
                                           hard_wrap: true)
    options = {
        :prettify => true,
        :fenced_code_blocks => true,
        :no_intra_emphasis => true,
        :autolink => true,
        :strikethrough => true,
        :lax_html_blocks => true,
        :superscript => true,
        :highlight => true,
        :disable_indented_code_blocks => false
    }
    markdown_to_html = Redcarpet::Markdown.new(renderer, options)
    markdown_to_html.render(text).html_safe
  end
end
